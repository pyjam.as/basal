import re as regulært_udtryk

uhøfligheder = [
    r"(?!Hendes Majestæt )[dD]ronning",
    r"(VARIABLE|COUNTER|[Ww]orld|[Hh]ello)",
]


def efterprøv_tagt_og_tone(line):
    for uhøflighed in uhøfligheder:
        match = regulært_udtryk.search(uhøflighed, line)
        if match != None:
            raise Exception(f'Det er meget uhøfligt af dig at skrive "{line}".')
