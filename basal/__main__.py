import argparse
import subprocess
from pathlib import Path

from . import byg


def get_parser():
    parser = argparse.ArgumentParser(
        prog="basal",
        description="Basal Basal oversætter",
    )
    subparsers = parser.add_subparsers(title="underkommandoer")
    parser_build = subparsers.add_parser(
        "byg", help="Oversæt et basalt programmel til maskinkode"
    )
    parser_build.add_argument("filnavn")
    parser_build.set_defaults(func=lambda args: byg(Path(args.filnavn)))
    parser_run = subparsers.add_parser("kør", help="Kør et basalt programmel")
    parser_run.add_argument("filnavn")
    parser_run.set_defaults(func=kør)
    return parser


def kør(args):
    out_filename = byg(Path(args.filnavn))
    if not out_filename:
        return
    subprocess.run([out_filename])


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    args.func(args)
