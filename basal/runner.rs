extern "C" {
    fn basal_main();
}

fn main() {
    unsafe { basal_main() }
}
