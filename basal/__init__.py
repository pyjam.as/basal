import sys
import subprocess
from pathlib import Path

from .ord import *
from .fortolker import ParseError, parse_line
from .spytter import Backend
from .slags import Slags
from .flueknep import FlueKnepFejl, flueknep


def byg(filename: Path) -> Path | None:
    asm_filename = filename.with_suffix(".s")
    obj_filename = filename.with_suffix(".o")
    out_filename = filename.with_suffix("")

    runtime_filename = Path(__file__).with_name("runtime.rs")
    runtime_obj_filename = Path(__file__).with_name("runtime.o")
    runner_filename = Path(__file__).with_name("runner.rs")

    ast = []
    errors = []

    with open(filename, "r") as f:
        for i, line in enumerate(f):
            if line == "\n":
                continue
            try:
                node = parse_line(i + 1, line.rstrip())
            except ParseError as e:
                errors.append((i + 1, e.args[0]))
            else:
                if node:
                    ast.append(node)
    if errors:
        for line_number, error in errors:
            print(f"Fejl på linje {line_number}: {error}")
        sys.exit(1)

    try:
        flueknep(ast)
    except FlueKnepFejl as e:
        print(e)
        sys.exit(1)

    backend = Backend.fra_ast(ast)

    with open(asm_filename, "w") as f:
        f.write(backend.full())

    result = subprocess.run(
        ["rustc", "--edition=2021", "-o", runtime_obj_filename, runtime_filename]
    )
    if result.returncode != 0:
        return

    if sys.platform.startswith("darwin"):
        asm_format = "macho64"
    elif sys.platform.startswith("win32"):
        print("Windows is not supported")
        sys.exit(1)
    else:
        asm_format = "elf64"
    result = subprocess.run(
        ["nasm", "-f", asm_format, "-o", obj_filename, asm_filename]
    )
    if result.returncode != 0:
        return

    result = subprocess.run(
        [
            "rustc",
            "--edition=2021",
            "-C",
            f"link-arg={obj_filename}",
            "-C",
            f"link-arg={runtime_obj_filename}",
            "-o",
            out_filename,
            runner_filename,
        ]
    )
    if result.returncode != 0:
        return

    return out_filename
