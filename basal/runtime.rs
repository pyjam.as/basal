//! The basal runtime.
#![crate_name = "basal_runtime"]
#![crate_type = "staticlib"]
#![cfg(target_pointer_width = "64")]
use core::ffi::c_void;
use core::fmt;
use core::slice;
use core::str;
use std::io::{self, Write};

// All strings and vectors used by the program.
//
// Currently never decremented nor deallocated.
static mut ALL_STRINGS: Vec<&'static str> = Vec::new();
static mut ALL_VECTORS: Vec<Vec<usize>> = Vec::new();

/// A string in Basal.
///
/// Currently an index into `ALL_STRINGS`.
#[repr(transparent)]
#[derive(Clone)]
struct BasalStr(usize);

impl BasalStr {
    fn from_static(s: &'static str) -> Self {
        let strings = unsafe { &mut ALL_STRINGS };
        let idx = strings.len();
        strings.push(s);
        Self(idx)
    }

    fn from_string(s: String) -> Self {
        let v: Vec<u8> = s.into();
        // Intentionally leak, for now
        let slice = v.leak();
        let s = str::from_utf8(slice).expect("non-utf8 string");
        Self::from_static(s)
    }

    fn as_str(&self) -> &str {
        unsafe { &ALL_STRINGS }
            .get(self.0)
            .unwrap_or_else(|| panic!("invalid string {}", self.0))
    }
}

impl fmt::Display for BasalStr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.as_str())
    }
}

#[no_mangle]
extern "C" fn basal_str_from_static(ptr: *const c_void) -> BasalStr {
    let ptr: *const usize = ptr.cast();
    let len = unsafe { ptr.read() } as usize;
    let ptr: *const u8 = ptr.cast();
    let ptr = unsafe { ptr.add(8) };
    let slice = unsafe { slice::from_raw_parts(ptr, len) };
    let s = str::from_utf8(slice).expect("non-utf8 string");
    BasalStr::from_static(s)
}

#[no_mangle]
extern "C" fn basal_str_print(s: BasalStr) {
    let mut stdout = io::stdout();
    write!(stdout, "{s}").unwrap();
    stdout.flush().unwrap();
}

#[no_mangle]
extern "C" fn basal_str_read() -> BasalStr {
    let mut buffer = String::new();
    let stdin = io::stdin();
    stdin
        .read_line(&mut buffer)
        .expect("failed reading from stdin");
    BasalStr::from_string(buffer.trim().to_string())
}

#[no_mangle]
extern "C" fn basal_str_concat(s1: BasalStr, s2: BasalStr) -> BasalStr {
    let s = format!("{s1}{s2}");
    BasalStr::from_string(s)
}

#[no_mangle]
extern "C" fn basal_str_len(s: BasalStr) -> isize {
    s.as_str().len() as isize
}

#[no_mangle]
extern "C" fn basal_str_to_int(s: BasalStr) -> isize {
    // Using common sentinel values for error handling, yay!
    s.as_str().trim().parse().unwrap_or(isize::MIN)
}

#[no_mangle]
extern "C" fn basal_str_eq(s1: BasalStr, s2: BasalStr) -> isize {
    (s1.as_str() == s2.as_str()) as isize
}

#[no_mangle]
extern "C" fn basal_str_gt(s1: BasalStr, s2: BasalStr) -> isize {
    (s1.as_str() > s2.as_str()) as isize
}

#[no_mangle]
extern "C" fn basal_str_lt(s1: BasalStr, s2: BasalStr) -> isize {
    (s1.as_str() < s2.as_str()) as isize
}

#[no_mangle]
extern "C" fn basal_str_contains(s: BasalStr, needle: BasalStr) -> isize {
    s.as_str().contains(needle.as_str()) as isize
}

#[no_mangle]
extern "C" fn basal_int_to_str(n: isize) -> BasalStr {
    BasalStr::from_string(format!("{n}"))
}

/// A vector in Basal.
///
/// Currently an index into `ALL_VECTORS`.
///
/// Allowed types are: `BasalStr` and `isize`, but the vector doesn't
/// really differentiate (since they have the same ABI).
#[repr(transparent)]
struct BasalVec(usize);

impl BasalVec {
    fn from_vec(v: Vec<usize>) -> Self {
        let vecs = unsafe { &mut ALL_VECTORS };
        let idx = vecs.len();
        vecs.push(v);
        Self(idx)
    }

    fn get(&self) -> &Vec<usize> {
        unsafe { &ALL_VECTORS }.get(self.0).expect("invalid vector")
    }

    // Unsound, but eh...
    fn get_mut(&self) -> &mut Vec<usize> {
        unsafe { &mut ALL_VECTORS }
            .get_mut(self.0)
            .expect("invalid vector")
    }
}

#[no_mangle]
extern "C" fn basal_vec_new() -> BasalVec {
    BasalVec::from_vec(Vec::new())
}

#[no_mangle]
extern "C" fn basal_vec_len(v: BasalVec) -> isize {
    v.get().len() as isize
}

#[no_mangle]
extern "C" fn basal_vec_int_eq(v1: BasalVec, v2: BasalVec) -> isize {
    // usize and isize compare equally, so just do that
    (v1.get() == v2.get()) as isize
}

#[no_mangle]
extern "C" fn basal_vec_str_eq(v1: BasalVec, v2: BasalVec) -> isize {
    let v1 = v1.get();
    let v2 = v2.get();
    if v1.len() != v2.len() {
        return false as isize;
    }
    v1.iter()
        .zip(v2)
        .all(|(&item1, &item2)| BasalStr(item1).as_str() == BasalStr(item2).as_str()) as isize
}

#[no_mangle]
extern "C" fn basal_vec_int_set(v: BasalVec, index: isize, item: isize) {
    let index = index as usize;
    let v = v.get_mut();
    // Fill the vector in case there are no elements
    while v.len() <= index {
        v.push(isize::MIN as usize);
    }
    v[index] = item as usize;
}

#[no_mangle]
extern "C" fn basal_vec_str_set(v: BasalVec, index: isize, item: BasalStr) {
    let index = index as usize;
    let v = v.get_mut();
    // Fill the vector in case there are no elements
    while v.len() <= index {
        v.push(BasalStr::from_static("").0);
    }
    v[index] = item.0;
}

#[no_mangle]
extern "C" fn basal_vec_int_get(v: BasalVec, idx: isize) -> isize {
    if let Some(val) = v.get().get(idx as usize) {
        *val as isize
    } else {
        isize::MIN
    }
}

#[no_mangle]
extern "C" fn basal_vec_str_get(v: BasalVec, idx: isize) -> BasalStr {
    if let Some(val) = v.get().get(idx as usize) {
        BasalStr(*val)
    } else {
        BasalStr::from_static("")
    }
}

#[no_mangle]
extern "C" fn basal_vec_concat(v1: BasalVec, v2: BasalVec) -> BasalVec {
    let mut vec = v1.get().clone();
    vec.extend(v2.get());
    BasalVec::from_vec(vec)
}

#[no_mangle]
extern "C" fn basal_vec_int_contains(v: BasalVec, needle: isize) -> isize {
    v.get().iter().any(|&item| item as isize == needle) as isize
}

#[no_mangle]
extern "C" fn basal_vec_str_contains(v: BasalVec, needle: BasalStr) -> isize {
    v.get()
        .iter()
        .any(|&item| BasalStr(item).as_str() == needle.as_str()) as isize
}
