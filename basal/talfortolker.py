import re as regulært_udtryk

Ingenting = None


def dan_regulært_udtryk_der_passer_på_grupper_baseret_på_en_liste(liste_af_strenge):
    return "(" + "|".join(liste_af_strenge) + ")"


tal_mindre_end_ti = {
    "nul": 0,
    "en": 1,
    "et": 1,
    "to": 2,
    "tre": 3,
    "fire": 4,
    "fem": 5,
    "seks": 6,
    "syv": 7,
    "otte": 8,
    "ni": 9,
}


tal_som_har_to_cifre_og_slutter_på_nul = {
    "ti": 10,
    "tyve": 20,
    "tredive": 30,
    "fyrre": 40,
    "fyrretyve": 40,
    "fyrretyvende": 40,
    "halvtreds": 50,
    "halvtredsindstyve": 50,
    "halvtredsindstyvende": 50,
    "tres": 60,
    "halvfjerds": 70,
    "firs": 80,
    "halvfems": 90,
}

tal_mellem_elleve_og_nitten = {
    "ti": 10,
    "elleve": 11,
    "tolv": 12,
    "tretten": 13,
    "fjorten": 14,
    "femten": 15,
    "seksten": 16,
    "sytten": 17,
    "atten": 18,
    "nitten": 19,
}


regex_der_finder_simple_tal = (
    dan_regulært_udtryk_der_passer_på_grupper_baseret_på_en_liste(
        tal_mindre_end_ti.keys()
    )
)

regex_til_tal_fra_nul_til_men_dog_ikke_med_tusind = (
    "(?="
    + dan_regulært_udtryk_der_passer_på_grupper_baseret_på_en_liste(
        list(tal_mindre_end_ti.keys())
        + list(tal_som_har_to_cifre_og_slutter_på_nul.keys())
        + list(tal_mellem_elleve_og_nitten.keys())
        + ["hundrede", "tusind"]
    )
    + ")"
    + "^((?P<hundredene>"
    + regex_der_finder_simple_tal
    + ")hundrede)?(og)?((?P<enerne>"
    + regex_der_finder_simple_tal
    + ")?(og(?P<tierne>"
    + dan_regulært_udtryk_der_passer_på_grupper_baseret_på_en_liste(
        tal_som_har_to_cifre_og_slutter_på_nul.keys()
    )
    + "))?|(?P<elverne>"
    + dan_regulært_udtryk_der_passer_på_grupper_baseret_på_en_liste(
        tal_mellem_elleve_og_nitten.keys()
    )
    + "))?$"
)

det_store_tal_regex = regulært_udtryk.sub(
    r"\?P<[a-z]+>", "", regex_til_tal_fra_nul_til_men_dog_ikke_med_tusind
)
det_store_tal_regex = regulært_udtryk.sub(r"\^", "", det_store_tal_regex)
det_store_tal_regex = regulært_udtryk.sub(r"\$", "", det_store_tal_regex)


def fortolk_tal(tal_i_form_af_tekst: str) -> int:
    resultat_af_at_bruge_mit_regulære_udtryk = regulært_udtryk.match(
        regex_til_tal_fra_nul_til_men_dog_ikke_med_tusind, tal_i_form_af_tekst
    )

    if resultat_af_at_bruge_mit_regulære_udtryk != Ingenting:
        tal = 0
        if resultat_af_at_bruge_mit_regulære_udtryk.group("elverne") != Ingenting:
            tal += tal_mellem_elleve_og_nitten[
                resultat_af_at_bruge_mit_regulære_udtryk.group("elverne")
            ]
        else:
            if resultat_af_at_bruge_mit_regulære_udtryk.group("enerne") != Ingenting:
                tal += tal_mindre_end_ti[
                    resultat_af_at_bruge_mit_regulære_udtryk.group("enerne")
                ]
            if resultat_af_at_bruge_mit_regulære_udtryk.group("tierne"):
                tal += tal_som_har_to_cifre_og_slutter_på_nul[
                    resultat_af_at_bruge_mit_regulære_udtryk.group("tierne")
                ]
            if tal > 10 and tal < 20:
                raise Exception(
                    "du skal vist have skolepengene tilbage. der er sgu ikke noget der hedder femogti"
                )
        if resultat_af_at_bruge_mit_regulære_udtryk.group("hundredene"):
            tal += (
                100
                * tal_mindre_end_ti[
                    resultat_af_at_bruge_mit_regulære_udtryk.group("hundredene")
                ]
            )
        return tal
    else:
        raise Exception(
            f'Dit tal "{tal_i_form_af_tekst}" er skrevet forkert. Lær at tale daskn.'
        )


def fortolk_ordenstal(tal_i_form_af_tekst: str) -> int | None:
    ordenstal_mindre_end_ti = {
        "første": 1,
        "andet": 2,
        "tredje": 3,
        "fjerde": 4,
        "femte": 5,
        "sjette": 6,
        "syvende": 7,
        "ottende": 8,
        "niende": 9,
        "tiende": 10,
    }
    return ordenstal_mindre_end_ti.get(tal_i_form_af_tekst)
