"""
Uses intel syntax.

Assembler: nasm

Compilation model:
- Single pass, per function.

ABI:
- Result of every expression is put in `rax`.
    - Either a string pointer or an integer.
- Function arguments are passed on the stack.

Runtime functions are implemented in `runtime.s`.
"""

from dataclasses import dataclass

from .ord import *
from .slags import Slags

Expr = Tal | Ref | Streng


class CompileError(Exception):
    pass


class Asm:
    """Private."""

    s: str

    def __init__(self, s: str):
        self.s = s

    def __iadd__(self, other):
        self.s += f"{other}\n"
        return self

    def label(self, label):
        self.s += f"{label}:\n"

    def instruction(self, instruction):
        self.s += f"    {instruction}\n"

    def __str__(self):
        return self.s

    def copy(self):
        return Asm(self.s)


def str_fmt_nasm(s: str) -> str:
    res = "db "
    is_normal = False

    for c in s:
        if c.isascii() and c.isprintable() and c != '"':
            if not is_normal:
                res += '"'
                is_normal = True
            res += c
        else:
            if is_normal:
                res += '", '
                is_normal = False
            res += f"{ord(c)}, "

    if is_normal:
        res += '", '
        is_normal = False

    return res.rstrip(", ")


class Backend:
    """
    A fairly good resource: https://web.stanford.edu/class/cs107/resources/x86-64-reference.pdf
    """

    # A mapping from variable name to stack address.
    local_variables: dict[str, int]

    asm: Asm
    static_strings: list[str]

    internal_label: int

    def fra_ast(ast: AST):
        backend = Backend()
        for linje in ast:
            backend.tilføj_linje(linje)

        return backend

    def __init__(self):
        self.local_variables = {}
        self.asm = Asm("")
        self.static_strings = []
        self.internal_label = 0

    def text(self) -> str:
        asm = Asm("")

        amount_of_local_variables = len(self.local_variables)
        if amount_of_local_variables % 2 == 0:
            # The stack is, when entering the frame, 16 + 8 bytes aligned.
            #
            # So if our local variables are a multiple of 2, we need to align
            # the stack to the 16 byte boundary.
            amount_of_local_variables += 1

        asm += (
            f"%define var_addr(idx) [rsp + ({amount_of_local_variables} - idx - 1) * 8]"
        )

        asm.label("prelude")

        for _ in range(amount_of_local_variables):
            asm.instruction("push rdx")

        asm.s += str(self.asm)

        asm.label("prologue")

        for _ in range(amount_of_local_variables):
            asm.instruction("pop rdx")

        asm.instruction("ret")

        return str(asm)

    def data(self) -> str:
        asm = Asm("")

        for i, s in enumerate(self.static_strings):
            asm.label(f"_static_str_{i}")
            asm.instruction(f"dq {len(s)}")
            if len(s) > 0:
                asm.instruction(str_fmt_nasm(s))

        return str(asm)

    def full(self) -> str:
        asm = Asm(
            """
DEFAULT rel

%ifidn __?OUTPUT_FORMAT?__, macho64
%define runtime_fn(x) _ %+ x
%else
%define runtime_fn(x) x
%endif

REQUIRED runtime_fn(basal_str_from_static)
REQUIRED runtime_fn(basal_str_print)
REQUIRED runtime_fn(basal_str_read)
REQUIRED runtime_fn(basal_str_concat)
REQUIRED runtime_fn(basal_str_len)
REQUIRED runtime_fn(basal_str_to_int)
REQUIRED runtime_fn(basal_str_eq)
REQUIRED runtime_fn(basal_str_gt)
REQUIRED runtime_fn(basal_str_lt)
REQUIRED runtime_fn(basal_str_contains)
REQUIRED runtime_fn(basal_int_to_str)

REQUIRED runtime_fn(basal_vec_new)
REQUIRED runtime_fn(basal_vec_len)
REQUIRED runtime_fn(basal_vec_int_eq)
REQUIRED runtime_fn(basal_vec_str_eq)
REQUIRED runtime_fn(basal_vec_int_set)
REQUIRED runtime_fn(basal_vec_str_set)
REQUIRED runtime_fn(basal_vec_int_get)
REQUIRED runtime_fn(basal_vec_str_get)
REQUIRED runtime_fn(basal_vec_concat)
REQUIRED runtime_fn(basal_vec_int_contains)
REQUIRED runtime_fn(basal_vec_str_contains)

SECTION .text
GLOBAL runtime_fn(basal_main)
runtime_fn(basal_main):
"""
        )
        asm.s += self.text()

        data = self.data()
        if data:
            asm += ""
            asm += "SECTION .data"
            asm.s += data

        return str(asm)

    def tilføj_linje(self, linje: Line):
        (_, label, påstand, betingelse) = linje
        if label:
            linje_nummer = label.name
        else:
            linje_nummer = None

        if linje_nummer is not None:
            self.asm.label(f"stmt_{linje_nummer}")

        if betingelse is not None:
            slags = self._expr(betingelse)
            assert slags == Slags.Tal
            self.asm.instruction("cmp rax, 0")
            self.internal_label += 1
            self.asm.instruction(f"je internal_label_{self.internal_label}")

        match påstand:
            case Sæt(ref, value):
                if value is not None:
                    slags = self._expr(value)
                    assert slags == ref.slags
                    self._sæt_forrige(ref)
                else:
                    assert ref.slags.er_beholder()
                    self.asm.instruction("call runtime_fn(basal_vec_new)")
                    self._sæt_forrige(ref)
            case Spørg(question, ref):
                self._expr(Streng(value=question))
                self.asm.instruction("mov rdi, rax")
                self.asm.instruction("call runtime_fn(basal_str_print)")
                self.asm.instruction("call runtime_fn(basal_str_read)")
                if ref.slags.er_tal():
                    self.asm.instruction("mov rdi, rax")
                    self.asm.instruction("call runtime_fn(basal_str_to_int)")
                elif ref.slags.er_beholder():
                    raise CompileError(
                        "kan ikke sætte svaret fra spørgsmål til beholdere"
                    )
                self._sæt_forrige(ref)
            case Skriv(output):
                for expr in output:
                    slags = self._expr(expr)
                    if slags.er_tal():
                        self.asm.instruction("mov rdi, rax")
                        self.asm.instruction("call runtime_fn(basal_int_to_str)")
                    elif slags.er_beholder():
                        raise CompileError("kan ikke skrive en beholder")
                    self.asm.instruction("mov rdi, rax")
                    self.asm.instruction("call runtime_fn(basal_str_print)")
                self._expr(Streng(value="\n"))
                self.asm.instruction("mov rdi, rax")
                self.asm.instruction("call runtime_fn(basal_str_print)")
                self.asm.instruction("mov rax, 0")
            case Gå(location):
                self.asm.instruction(f"jmp stmt_{location}")
            case Læg(ref, value):
                # ref = ref + value
                expr_slags = self._expr(value)
                assert expr_slags == ref.slags
                if ref.slags.er_tal():
                    self.asm.instruction(f"mov rdx, {self._ref_addr(ref)}")
                    self.asm.instruction("add rax, rdx")
                    self.asm.instruction(f"mov {self._ref_addr(ref)}, rax")
                elif ref.slags.er_streng() or ref.slags.er_beholder():
                    self.asm.instruction(f"mov rdi, {self._ref_addr(ref)}")
                    self.asm.instruction("mov rsi, rax")
                    if ref.slags.er_beholder():
                        funktionsnavn = "runtime_fn(basal_vec_concat)"
                    else:
                        funktionsnavn = "runtime_fn(basal_str_concat)"
                    self.asm.instruction(f"call {funktionsnavn}")
                    self.asm.instruction(f"mov {self._ref_addr(ref)}, rax")
                else:
                    raise CompileError(f"ukendt slags {ref.slags}")

            case Træk(ref, value):
                # ref = ref - value
                expr_slags = self._expr(value)
                assert expr_slags == ref.slags
                if not ref.slags.er_tal():
                    raise CompileError("kan kun trække tal fra hinanden")
                self.asm.instruction(f"mov rdx, {self._ref_addr(ref)}")
                self.asm.instruction("sub rdx, rax")
                self.asm.instruction(f"mov {self._ref_addr(ref)}, rdx")
                self.asm.instruction("mov rax, rdx")

            case Indsæt(ref, index, value):
                if not ref.slags.er_beholder():
                    raise CompileError("kan kun indsætte ting i beholdere")

                index_slags = self._expr(index)
                assert index_slags == Slags.Tal
                # Temporarily store. Can get overwritten by the `value` expression, but eh.
                self.asm.instruction(f"mov rcx, rax")

                expr_slags = self._expr(value)
                assert expr_slags == ref.slags.indeholder()

                if ref.slags == Slags.TalBeholder:
                    funktionsnavn = "runtime_fn(basal_vec_int_set)"
                else:
                    funktionsnavn = "runtime_fn(basal_vec_str_set)"

                self.asm.instruction(f"mov rdi, {self._ref_addr(ref)}")  # Første
                self.asm.instruction("mov rsi, rcx")  # Anden
                self.asm.instruction("mov rdx, rax")  # Tredje
                self.asm.instruction(f"call {funktionsnavn}")
                self.asm.instruction(f"mov rax, 0")

            case Læs(ref, index, beholder):
                if not beholder.slags.er_beholder():
                    raise CompileError("kan kun læse ting fra beholdere")
                if beholder.slags.indeholder() != ref.slags:
                    raise CompileError("kan kun læse ting ud i samme type variabler")

                index_slags = self._expr(index)
                if index_slags != Slags.Tal:
                    raise CompileError("kan kun læse via tal indeks")

                if beholder.slags == Slags.TalBeholder:
                    funktionsnavn = "runtime_fn(basal_vec_int_get)"
                else:
                    funktionsnavn = "runtime_fn(basal_vec_str_get)"

                self.asm.instruction(f"mov rdi, {self._ref_addr(beholder)}")  # Første
                self.asm.instruction("mov rsi, rax")  # Anden
                self.asm.instruction(f"call {funktionsnavn}")
                self._sæt_forrige(ref)

            case _:
                raise CompileError(f"ukendt påstand: {påstand}")

        if betingelse is not None:
            self.asm.label(f"internal_label_{self.internal_label}")

    def _ref_addr(self, ref: Ref) -> str:
        if ref.name in self.local_variables:
            var_stack_position = self.local_variables[ref.name]
            return f"var_addr({var_stack_position})"
        return "unset"  # To give a bit better error messages

    def _sæt_forrige(self, ref: Ref):
        addr = self._ref_addr(ref)
        if addr == "unset":
            self.local_variables[ref.name] = len(self.local_variables)
            addr = self._ref_addr(ref)
        self.asm.instruction(f"mov {addr}, rax")

    def _expr(self, expr: Expr) -> Slags:
        match expr:
            case Ref(ref, slags):
                self.asm.instruction(f"mov rax, {self._ref_addr(expr)}")
                return slags
            case Streng(value):
                try:
                    idx = self.static_strings.index(value)
                except ValueError:
                    idx = len(self.static_strings)
                    self.static_strings.append(value)
                self.asm.instruction(f"lea rdi, [rel _static_str_{idx}]")
                self.asm.instruction("call runtime_fn(basal_str_from_static)")
                return Slags.Streng
            case Tal(value):
                self.asm.instruction(f"mov rax, {value}")
                return Slags.Tal
            case Længde(arg):
                slags = self._expr(arg)
                if slags.er_streng():
                    funktionsnavn = "runtime_fn(basal_str_len)"
                elif slags.er_beholder():
                    funktionsnavn = "runtime_fn(basal_vec_len)"
                else:
                    raise CompileError("kan ikke finde længden af tal")
                self.asm.instruction(f"mov rdi, rax")
                self.asm.instruction(f"call {funktionsnavn}")
                return Slags.Tal
            # case Funktionskald(funktionsnavn, argument_exprs):
            #     self._funktionskald(funktionsnavn, argument_exprs)
            case Sammenligning(negativ, op, venstre, højre):
                venstre_slags = self._expr(venstre)
                # We don't support nested "hvis" expressions, so we can just
                # temporarily store the result in rcx.
                self.asm.instruction(f"mov rcx, rax")
                højre_slags = self._expr(højre)

                if venstre_slags.er_tal():
                    assert venstre_slags == højre_slags
                    # See <https://www.felixcloutier.com/x86/setcc>
                    #
                    # In particular, note that we use greater/less comparisons
                    # instead of above/below; that's because we have signed
                    # integer semantics.
                    self.asm.instruction("cmp rax, rcx")
                    match (negativ, op):
                        case (False, "=="):
                            self.asm.instruction("sete al")
                        case (True, "=="):
                            self.asm.instruction("setne al")
                        case (False, ">") | (True, "<"):
                            self.asm.instruction("setg al")
                        case (False, "<") | (True, ">"):
                            self.asm.instruction("setl al")
                        case _:
                            raise CompileError(f"usupporteret operation {op}, {expr!r}")
                elif venstre_slags.er_streng():
                    assert venstre_slags == højre_slags
                    self.asm.instruction("mov rdi, rax")
                    self.asm.instruction("mov rsi, rcx")
                    match op:
                        case "==":
                            self.asm.instruction("call runtime_fn(basal_str_eq)")
                        case ">":
                            self.asm.instruction("call runtime_fn(basal_str_gt)")
                        case "<":
                            self.asm.instruction("call runtime_fn(basal_str_lt)")
                        case "in":
                            self.asm.instruction("call runtime_fn(basal_str_contains)")
                        case _:
                            raise CompileError(f"usupporteret operation {op}, {expr!r}")

                    if negativ:
                        self.asm.instruction("not rax")
                elif venstre_slags.er_beholder():
                    assert venstre_slags.indeholder() == højre_slags
                    self.asm.instruction("mov rdi, rax")
                    self.asm.instruction("mov rsi, rcx")
                    match (venstre_slags, op):
                        case (Slags.TalBeholder, "=="):
                            self.asm.instruction("call runtime_fn(basal_vec_int_eq)")
                        case (Slags.TalBeholder, "in"):
                            self.asm.instruction(
                                "call runtime_fn(basal_vec_int_contains)"
                            )
                        case (Slags.StrengBeholder, "=="):
                            self.asm.instruction("call runtime_fn(basal_vec_str_eq)")
                        case (Slags.StrengBeholder, "in"):
                            self.asm.instruction(
                                "call runtime_fn(basal_vec_str_contains)"
                            )
                        case _:
                            raise CompileError(f"usupporteret operation {op}, {expr!r}")
                    if negativ:
                        self.asm.instruction("not rax")
                else:
                    raise CompileError(f"ukendt slags {venstre_slags}")
                return Slags.Tal
            case _:
                raise CompileError(f"ukendt: {expr}")

    def _funktionskald(self, funktionsnavn: str, argument_exprs: list[Expr]):
        for arg_expr in argument_exprs:
            self._expr(arg_expr)
            self.asm.instruction("push rax")

        # TODO: Ensure stack alignment
        self.asm.instruction(f"call _{funktionsnavn}")

        for _ in argument_exprs:
            self.asm.instruction("pop rdx")
