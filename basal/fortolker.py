import re

from .ord import *
from .slags import Slags
from .talfortolker import det_store_tal_regex
from .talfortolker import fortolk_tal
from .talfortolker import fortolk_ordenstal
from .emmagad import efterprøv_tagt_og_tone


class ParseError(Exception):
    pass


_tal = det_store_tal_regex
_ref = r"([A-ZÆØÅ]+)"
_streng = r'(\".*?")'
_atom = f"({_ref}|{_streng}|{_tal})"


def få_slags(o: Streng | Tal | Ref) -> Slags:
    if isinstance(o, Streng):
        return Slags.Streng
    if isinstance(o, Tal):
        return Slags.Tal
    if isinstance(o, Ref):
        return o.slags
    raise TypeError("")


def _match_atom(text, type_for_ref):
    result = None
    if m := re.match(_tal, text):
        result = Tal(value=fortolk_tal(m.group(0)))
    elif m := re.match(_streng, text):
        result = Streng(value=m.group(0)[1:-1])
    elif m := re.match(_ref, text):
        assert type_for_ref != None
        result = Ref(name=å(m.group(0)), slags=type_for_ref)
    else:
        raise Exception(
            f'Jeg kan ikke helt regne ud hvad det er for en slags "{text}" skal være. Beklager kammerat.'
        )
    return result, text[m.end() + 1 :]


def _parse_boolsk_udtryk(line):
    "Fortolker et boolsk udtryk, med den andtagelse at linjen starter og slutter derpå"
    if line.startswith("længden af "):
        arg, text = _match_atom(line[11:], Slags.Streng)
        if arg is None:
            raise ParseError("længden af mangler noget at tage længden af")
        line = text
        left = Længde(arg=arg)
    else:
        left, line = _match_atom(line, Slags.Afledt)
        if left is None:
            raise ParseError(
                'der mangler noget efter "hvis" f.eks: hvis N er mindre end 10'
            )

    op_match = re.match(
        r"(?P<negativ>ikke )?(?P<op>indeholder|er mindre end|er større end|er lig) ",
        line,
    )
    if op_match is None:
        raise ParseError("ugyldig sammenligning")
    negativ = op_match.group("negativ") is not None
    match op_match.group("op"):
        case "indeholder":
            op = "in"
        case "er mindre end":
            op = "<"
        case "er større end":
            op = ">"
        case "er lig":
            op = "=="
        case _:
            raise ParseError("ukendt operator")
    line = line[op_match.end() :]

    if op == "in":
        slags = Slags.Streng
    elif op in ("<", ">"):
        slags = Slags.Tal
    else:
        slags = Slags.Afledt

    right, remainder = _match_atom(line, slags)

    assert (
        len(remainder) == 0
    ), "Du kan ikke bare smide alt muligt bøvl til sidst i dine linjer"

    if right is None:
        raise ParseError("slutningen af linjen er ugyldig: {line!r}")

    if isinstance(left, Ref):
        left.slags = slags
        if slags == Slags.Afledt:
            left.slags = få_slags(right)

    return Sammenligning(negativ=negativ, op=op, venstre=left, højre=right)


def å(aa):
    return aa.replace("AA", "Å")


def _parse_hvis(line):
    if line == "":
        return None
    if not line.startswith(" hvis "):
        raise ParseError(
            f'slutningen af linjen giver ikke mening, den skal starte med "hvis". Da vi regnede med at se "hvis", så kom du bare med "{line}", og det fandt vi altså temmeligt forvirrende her på kontoret.'
        )
    return _parse_boolsk_udtryk(line[6:])


def parse_line(line_number, line) -> Line | None:
    efterprøv_tagt_og_tone(line)
    if line.startswith("fodnote: "):
        return None
    sæt = f"sæt (?P<ref>{_ref}) til (?P<value>en talbeholder|en tekstbeholder|{_atom})"
    spørg = f"spørg (?P<streng>{_streng}) og husk svaret som (tallet )?(?P<ref>{_ref})"
    skriv = f"skriv (?P<first>{_atom})( {_atom})*"
    gå = f"gå (?P<location>til [a-zæøå]+)"
    læg = f"læg (?P<value>{_atom}) til (?P<ref>{_ref})"
    træk = f"træk (?P<value>{_atom}) fra (?P<ref>{_ref})"

    if line.startswith(" "):
        line = line.lstrip()
        label = None
        if line.startswith("fodnote: "):
            return None
    else:
        label_match = re.match(f"Som det (?P<label>[a-zæøå]+) ", line)
        if label_match is None:
            raise ParseError('linjer skal starte med "Som det ORDENSTAL"')
        mærkat = label_match.group("label")
        if fortolk_ordenstal(mærkat) is None:
            raise ParseError(f"{mærkat} er ikke et gyldigt ordenstal")
        label = Label(name=mærkat)
        line = line[label_match.end() :]

    sæt_match = re.match(sæt, line)
    if sæt_match:
        match = sæt_match.group("value")

        if match == "en talbeholder":
            value = None
            slags = Slags.TalBeholder
        elif match == "en tekstbeholder":
            value = None
            slags = Slags.TekstBeholder
        else:
            value, _ = _match_atom(match, Slags.Afledt)
            slags = få_slags(value)

        hvis = _parse_hvis(line[sæt_match.end() :])
        return (
            line_number,
            label,
            Sæt(ref=Ref(name=å(sæt_match.group("ref")), slags=slags), value=value),
            hvis,
        )
    elif match := re.match(
        f"sæt (?P<index>{_tal}|{_ref}) i (?P<ref>{_ref}) til (?P<value>{_atom})", line
    ):
        index_unparsed = match.group("index")
        try:
            index = Tal(fortolk_tal(index_unparsed))
        except:
            index = Ref(å(index_unparsed), Slags.Tal)
        værdi, _ = _match_atom(match.group("value"), Slags.Afledt)
        hvis = _parse_hvis(line[match.end() :])
        return (
            line_number,
            label,
            Indsæt(
                ref=Ref(name=å(match.group("ref")), slags=Slags.Afledt),
                index=index,
                value=værdi,
            ),
            hvis,
        )
    elif match := re.match(
        f"sæt (?P<ref>{_ref}) til element (?P<index>{_tal}|{_ref}) fra (?P<beholder>{_ref})",
        line,
    ):
        index_unparsed = match.group("index")
        try:
            index = Tal(fortolk_tal(index_unparsed))
        except:
            index = Ref(å(index_unparsed), Slags.Tal)
        hvis = _parse_hvis(line[match.end() :])
        return (
            line_number,
            label,
            Læs(
                ref=Ref(name=å(match.group("ref")), slags=Slags.Afledt),
                index=index,
                beholder=Ref(name=å(match.group("beholder")), slags=Slags.Afledt),
            ),
            hvis,
        )
    elif line.startswith("sæt"):
        raise ParseError("sæt-linjer skal følge formattet: sæt NAVN til VÆRDI")

    spørg_match = re.match(spørg, line)
    if spørg_match:
        hvis = _parse_hvis(line[spørg_match.end() :])

        slags = Slags.Streng
        if spørg_match.groups()[2] == "tallet ":
            slags = Slags.Tal

        return (
            line_number,
            label,
            Spørg(
                question=spørg_match.group("streng")[1:-1],
                store=Ref(name=å(spørg_match.group("ref")), slags=slags),
            ),
            hvis,
        )
    elif line.startswith("spørg"):
        raise ParseError(
            "spørg-linjer skal følge formattet: spørg TEKST og husk svaret som NAVN"
        )

    skriv_match = re.match(skriv, line)
    if skriv_match:
        output = []
        match = skriv_match.group(0)[6:]

        while match:
            token, match = _match_atom(match, Slags.Afledt)
            output.append(token)

        hvis = _parse_hvis(line[skriv_match.end() :])
        return (line_number, label, Skriv(output=output), hvis)
    elif line.startswith("skriv"):
        raise ParseError("skriv skal være efterfulgt af en eller flere ting at skrive")

    gå_match = re.match(gå, line)
    if gå_match:
        location = gå_match.group("location").removeprefix("til ")
        if fortolk_ordenstal(location) is None:
            raise ParseError(f"{location} er ikke et ordenstal")
        hvis = _parse_hvis(line[gå_match.end() :])
        return (
            line_number,
            label,
            Gå(location=location),
            hvis,
        )
    elif line.startswith("gå"):
        raise ParseError("man kan kun gå til ordenstal")

    læg_match = re.match(læg, line)
    if læg_match:
        ref = læg_match.group("ref")
        value, _ = _match_atom(læg_match.group("value"), Slags.Afledt)
        hvis = _parse_hvis(line[læg_match.end() :])
        return (
            line_number,
            label,
            Læg(ref=Ref(name=å(ref), slags=få_slags(value)), value=value),
            hvis,
        )
    elif line.startswith("læg"):
        raise ParseError("læg-linjer skal følge formattet: læg N til NAME")

    træk_match = re.match(træk, line)
    if træk_match:
        ref = træk_match.group("ref")
        value, _ = _match_atom(træk_match.group("value"), Slags.Afledt)
        hvis = _parse_hvis(line[træk_match.end() :])
        return (
            line_number,
            label,
            Træk(ref=Ref(name=å(ref), slags=Slags.Tal), value=value),
            hvis,
        )
    elif line.startswith("træk"):
        raise ParseError("træk-linjer skal følge formattet: træk N fra NAME")
    elif line.startswith("hævd at "):
        # TODO: hvis udtryk under hævd
        return (line_number, label, Hævdelse(_parse_boolsk_udtryk(line[8:])), None)
    raise ParseError("du skal bruge sæt, spørg, skriv, gå, læg eller træk")
