from enum import Enum


class Slags(Enum):
    Tal = 1
    Streng = 2
    TalBeholder = 3
    TekstBeholder = 4
    # Der er ikke nogle afledte efter der er blevet fluekneppet
    Afledt = 5

    def er_tal(self) -> bool:
        return self == Slags.Tal

    def er_streng(self) -> bool:
        return self == Slags.Streng

    def er_beholder(self) -> bool:
        return (self == Slags.TalBeholder) or (self == Slags.TekstBeholder)

    def indeholder(self):
        match self:
            case Slags.TalBeholder:
                return Slags.Tal
            case Slags.StrengBeholder:
                return Slags.Streng
            case _:
                return None
