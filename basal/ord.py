from dataclasses import dataclass
from typing import Literal

from . import slags


@dataclass
class Label:
    name: str


@dataclass
class Tal:
    value: int


@dataclass
class Ref:
    name: str
    slags: slags.Slags


@dataclass
class Streng:
    value: str


@dataclass
class Sæt:
    ref: Ref
    value: Streng | Tal | Ref | None  # None betyder at det er en beholder


@dataclass
class Spørg:
    question: str
    store: Ref


@dataclass
class Skriv:
    output: list[Tal | Ref | Streng]


@dataclass
class Gå:
    location: str


@dataclass
class Læg:
    ref: Ref
    value: Streng | Tal | Ref


@dataclass
class Træk:
    ref: Ref
    value: Streng | Tal | Ref


@dataclass
class Længde:
    arg: Ref | Tal | Streng


@dataclass
class Indsæt:
    ref: Ref
    index: Tal | Ref
    value: Streng | Tal | Ref


@dataclass
class Læs:
    ref: Ref
    index: Tal | Ref
    beholder: Ref


@dataclass
class Sammenligning:
    negativ: bool
    op: Literal["==", ">", "<", "in"]
    venstre: Længde | Ref | Tal | Streng
    højre: Ref | Tal | Streng


@dataclass
class Hævdelse:
    stridspunkt: Sammenligning


Linjenummer = int
Påstand = Sæt | Spørg | Skriv | Gå | Læg | Træk | Indsæt | Læs
# TODO: Indsæt Hævdelse
Line = tuple[Linjenummer, Label | None, Påstand, Sammenligning | None]
AST = list[Line]
