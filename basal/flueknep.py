from . import ord as ast
from .slags import Slags


class FlueKnepFejl(Exception):
    pass


def få_slags(v):
    if isinstance(v, ast.Streng):
        return Slags.Streng
    if isinstance(v, ast.Tal):
        return Slags.Tal
    if isinstance(v, ast.Ref):
        return v.slags
    raise Exception("du kan ikke finde ud af at kode")


def beholder_til_element_slags(beholder):
    if beholder.slags == Slags.TalBeholder:
        return Slags.Tal
    if beholder.slags == Slags.TekstBeholder:
        return Slags.Streng
    raise Exception("vores flueknepper er dårlig")


def flueknep(linjer: ast.AST):
    slags_opslag: dict[str, tuple[Slags, int]] = {}
    brugte_mærkater = set()

    def flueknep_eller_opdater(linje, i, navn, slags):
        if navn in slags_opslag:
            original_slags, linje = slags_opslag[navn]
            if original_slags != slags:
                raise FlueKnepFejl(
                    f"{navn} bliver brugt som flere ting, se linje {linje} og {i}"
                )
        else:
            slags_opslag[navn] = (slags, i)

    for _, linje, påstand, __ in linjer[:1]:
        if linje is None or linje.name != "første":
            raise FlueKnepFejl("Den første kommando skal starte med: Som det første")

    # populer slags opslaget
    for i, linje, påstand, __ in linjer:
        if linje is not None:
            if linje.name in brugte_mærkater:
                raise FlueKnepFejl(f"Mærkatet {linje.name} bliver brugt flere gange")
            else:
                brugte_mærkater.add(linje.name)
        match påstand:
            case ast.Sæt(ref=ast.Ref(navn, slags)):
                if slags == Slags.Afledt:
                    continue
                flueknep_eller_opdater(linje, i, navn, slags)
            case ast.Spørg(store=ast.Ref(navn, slags)):
                flueknep_eller_opdater(linje, i, navn, slags)
            case ast.Læs(_, index, __):
                if isinstance(index, ast.Tal):
                    continue
                flueknep_eller_opdater(linje, i, index.name, index.slags)
            case ast.Indsæt(_, ast.Ref(name, slags), __):
                flueknep_eller_opdater(linje, i, name, slags)

    for i, linje, påstand, __ in linjer:
        match påstand:
            case ast.Sæt(ref, other):
                if not isinstance(other, ast.Ref):
                    continue
                slags = slags_opslag[other.name][0]
                flueknep_eller_opdater(linje, i, ref.name, slags)
                ref.slags = slags
                other.slags = slags
            case ast.Læs(ref, _, beholder):
                beholder.slags = slags_opslag[beholder.name][0]
                if beholder.slags == Slags.TalBeholder:
                    slags_opslag[ref.name] = (Slags.Tal, i)
                    ref.slags = Slags.Tal
                if beholder.slags == Slags.TekstBeholder:
                    slags_opslag[ref.name] = (Slags.Streng, i)
                    ref.slags = Slags.Streng
            case ast.Læg(_, ref):
                if isinstance(ref, ast.Ref):
                    ref.slags = slags_opslag[ref.name][0]

    for i, _, påstand, sammenligning in linjer:
        match påstand:
            case ast.Læg(ast.Ref(navn, _), værdi):
                if navn not in slags_opslag:
                    raise FlueKnepFejl(f"{navn} på linje {i} er ikke defineret")
                slags, __ = slags_opslag[navn]
                påstand.ref.slags = slags
                if få_slags(værdi) != slags:
                    raise FlueKnepFejl(
                        f"{navn} bliver brugt som tal og tekst, se linje {i}"
                    )
            case ast.Træk(ast.Ref(name, _), værdi):
                if få_slags(værdi) != Slags.Tal:
                    raise FlueKnepFejl(f"man kan kun trække tal fra tal, se linje {i}")
            case ast.Skriv(liste):
                for atom in liste:
                    if isinstance(atom, ast.Ref):
                        atom.slags = slags_opslag[atom.name][0]
            case ast.Indsæt(ref, _, value):
                ref.slags = slags_opslag[ref.name][0]
                if ref.slags not in (Slags.TalBeholder, Slags.TekstBeholder):
                    raise FlueKnepFejl(
                        f"{ref.name} skal være en beholder før man kan indsætte elementer i den"
                    )
                if isinstance(value, ast.Ref):
                    value.slags = beholder_til_element_slags(ref)
                slags = få_slags(value)
                if ref.slags == Slags.TalBeholder and slags != Slags.Tal:
                    raise FlueKnepFejl(f"man kan kun indsætte tal i {ref.name}")
                if ref.slags == Slags.TekstBeholder and slags != Slags.Streng:
                    raise FlueKnepFejl(f"man kan kun indsætte tekst i {ref.name}")
            case ast.Sæt(ref=ref, value=mulig_ref):
                if not isinstance(mulig_ref, ast.Ref):
                    continue
                slags = slags_opslag[mulig_ref.name][0]
                ref.slags = slags
                mulig_ref.slags = slags

        if sammenligning is None:
            continue

        if isinstance(sammenligning.venstre, ast.Ref):
            sammenligning.venstre.slags = slags_opslag[sammenligning.venstre.name][0]

        if isinstance(sammenligning.højre, ast.Ref):
            sammenligning.højre.slags = slags_opslag[sammenligning.højre.name][0]

        if isinstance(sammenligning.venstre, ast.Længde):
            if få_slags(sammenligning.venstre.arg) not in (
                Slags.Streng,
                Slags.TalBeholder,
                Slags.TekstBeholder,
            ):
                raise FlueKnepFejl(
                    f"man kan kun tage længden af tekst og beholdere, se linje {i}"
                )

        if sammenligning.op in (">", "<"):
            if (
                få_slags(sammenligning.venstre) != Slags.Tal
                or få_slags(sammenligning.højre) != Slags.Tal
            ):
                raise FlueKnepFejl(
                    f"man kan kun sammenligne størrelsen af tal, se linje {i}"
                )

        if sammenligning.op == "==" and få_slags(sammenligning.venstre) != få_slags(
            sammenligning.højre
        ):
            raise FlueKnepFejl(f"man kan ikke sammenligne tal med tekst, se linje {i}")
