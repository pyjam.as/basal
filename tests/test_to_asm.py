from basal import *


def assert_ast(ast, text, data=""):
    backend = Backend.fra_ast([(None, x, y, z) for (x, y, z) in ast])
    assert backend.text().strip() == text.strip()
    assert backend.data().strip() == data.strip()


def test_sæt():
    ast = [
        (
            Label(name=1),
            Sæt(ref=Ref(name="abc", slags=Slags.Tal), value=Tal(value=16)),
            None,
        ),
    ]
    text = """
%define var_addr(idx) [rsp + (1 - idx - 1) * 8]
prelude:
    push rdx
stmt_1:
    mov rax, 16
    mov var_addr(0), rax
prologue:
    pop rdx
    ret
"""
    assert_ast(ast, text)


def test_sæt_tidligere():
    ast = [
        (
            Label(name=1),
            Sæt(ref=Ref(name="abc", slags=Slags.Tal), value=Tal(value=111)),
            None,
        ),
        (
            Label(name=2),
            Sæt(ref=Ref(name="abc", slags=Slags.Tal), value=Tal(value=222)),
            None,
        ),
        (
            Label(name=3),
            Sæt(ref=Ref(name="def", slags=Slags.Tal), value=Tal(value=333)),
            None,
        ),
        (
            Label(name=4),
            Sæt(ref=Ref(name="abc", slags=Slags.Tal), value=Tal(value=444)),
            None,
        ),
        (
            Label(name=5),
            Sæt(ref=Ref(name="def", slags=Slags.Tal), value=Tal(value=555)),
            None,
        ),
    ]
    text = """
%define var_addr(idx) [rsp + (3 - idx - 1) * 8]
prelude:
    push rdx
    push rdx
    push rdx
stmt_1:
    mov rax, 111
    mov var_addr(0), rax
stmt_2:
    mov rax, 222
    mov var_addr(0), rax
stmt_3:
    mov rax, 333
    mov var_addr(1), rax
stmt_4:
    mov rax, 444
    mov var_addr(0), rax
stmt_5:
    mov rax, 555
    mov var_addr(1), rax
prologue:
    pop rdx
    pop rdx
    pop rdx
    ret
"""
    assert_ast(ast, text)


def test_gå_hvis():
    ast = [
        (
            Label(name=1),
            Gå(location=1),
            Sammenligning(
                negativ=False, op="==", venstre=Tal(value=0), højre=Tal(value=1)
            ),
        ),
    ]
    text = """
%define var_addr(idx) [rsp + (1 - idx - 1) * 8]
prelude:
    push rdx
stmt_1:
    mov rax, 0
    mov rcx, rax
    mov rax, 1
    cmp rax, rcx
    sete al
    cmp rax, 0
    je internal_label_1
    jmp stmt_1
internal_label_1:
prologue:
    pop rdx
    ret
"""
    assert_ast(ast, text)


def test_skriv():
    ast = [
        (Label(name=1), Skriv(output=[Streng(value="test")]), None),
    ]
    text = """
%define var_addr(idx) [rsp + (1 - idx - 1) * 8]
prelude:
    push rdx
stmt_1:
    lea rdi, [rel _static_str_0]
    call runtime_fn(basal_str_from_static)
    mov rdi, rax
    call runtime_fn(basal_str_print)
    lea rdi, [rel _static_str_1]
    call runtime_fn(basal_str_from_static)
    mov rdi, rax
    call runtime_fn(basal_str_print)
    mov rax, 0
prologue:
    pop rdx
    ret
"""
    data = """
_static_str_0:
    dq 4
    db "test"
_static_str_1:
    dq 1
    db 10
"""
    assert_ast(ast, text, data)


def test_spørg():
    ast = [
        (
            Label(name=1),
            Spørg(question="Hello, world!\n", store=Ref("abc", slags=Slags.Streng)),
            None,
        ),
    ]
    text = """
%define var_addr(idx) [rsp + (1 - idx - 1) * 8]
prelude:
    push rdx
stmt_1:
    lea rdi, [rel _static_str_0]
    call runtime_fn(basal_str_from_static)
    mov rdi, rax
    call runtime_fn(basal_str_print)
    call runtime_fn(basal_str_read)
    mov var_addr(0), rax
prologue:
    pop rdx
    ret
"""
    data = """
_static_str_0:
    dq 14
    db "Hello, world!", 10
"""
    assert_ast(ast, text, data)
