from basal.ord import *
from basal.fortolker import parse_line
from basal.talfortolker import fortolk_tal, fortolk_ordenstal
from basal.slags import Slags

import pytest


def test_gå_til_ti():
    assert parse_line(1, "Som det første gå til tiende") == (
        1,
        Label(name="første"),
        Gå(location="tiende"),
        None,
    )


def test_sæt_streng():
    assert parse_line(1, 'Som det første sæt N til "Hej"') == (
        1,
        Label(name="første"),
        Sæt(ref=Ref("N", Slags.Streng), value=Streng(value="Hej")),
        None,
    )


def test_skriv_simpel():
    assert parse_line(1, 'Som det første skriv "Hej, Verden!"') == (
        1,
        Label(name="første"),
        Skriv(output=[Streng(value="Hej, Verden!")]),
        None,
    )


def test_spørg():
    assert parse_line(
        1, 'Som det første spørg "Hvad hedder du? " og husk svaret som NAVN'
    ) == (
        1,
        Label(name="første"),
        Spørg(question="Hvad hedder du? ", store=Ref(name="NAVN", slags=Slags.Streng)),
        None,
    )


def test_skriv_hej_var():
    assert parse_line(1, 'Som det første skriv "Hej " NAVN') == (
        1,
        Label(name="første"),
        Skriv(output=[Streng(value="Hej "), Ref(name="NAVN", slags=Slags.Afledt)]),
        None,
    )


def test_spørg_to():
    assert parse_line(
        1,
        'Som det første spørg "Hvor mange stjerner vil du have? " og husk svaret som N',
    ) == (
        1,
        Label(name="første"),
        Spørg(
            question="Hvor mange stjerner vil du have? ",
            store=Ref(name="N", slags=Slags.Streng),
        ),
        None,
    )


def test_sæt_tom_streng():
    assert parse_line(1, 'Som det første sæt S til ""') == (
        1,
        Label(name="første"),
        Sæt(ref=Ref("S", Slags.Streng), value=Streng(value="")),
        None,
    )


def test_skriv_tre_args():
    assert parse_line(1, 'Som det første skriv "Hej " NAVN fire') == (
        1,
        Label(name="første"),
        Skriv(
            output=[
                Streng(value="Hej "),
                Ref(name="NAVN", slags=Slags.Afledt),
                Tal(value=4),
            ]
        ),
        None,
    )


def test_større_end():
    assert parse_line(1, "Som det tiende gå til niende hvis N er større end to") == (
        1,
        Label(name="tiende"),
        Gå(location="niende"),
        Sammenligning(
            negativ=False,
            op=">",
            venstre=Ref(name="N", slags=Slags.Tal),
            højre=Tal(value=2),
        ),
    )


def test_længden_af():
    # TODO: Den kan ikke parse 202
    assert parse_line(
        1, 'Som det første skriv "Hej, Verden!" hvis længden af A er lig tohundredeogto'
    ) == (
        1,
        Label(name="første"),
        Skriv(output=[Streng(value="Hej, Verden!")]),
        Sammenligning(
            negativ=False,
            op="==",
            venstre=Længde(arg=Ref(name="A", slags=Slags.Streng)),
            højre=Tal(value=202),
        ),
    )


def test_sæt_med_betingelse():
    assert parse_line(
        1, 'Som det ottende sæt S til "Hej, Verden!" hvis S indeholder "tekst"'
    ) == (
        1,
        Label(name="ottende"),
        Sæt(ref=Ref("S", Slags.Streng), value=Streng(value="Hej, Verden!")),
        Sammenligning(
            negativ=False,
            op="in",
            venstre=Ref(name="S", slags=Slags.Streng),
            højre=Streng(value="tekst"),
        ),
    )


def test_læg():
    assert parse_line(1, "Som det første læg tre til N") == (
        1,
        Label(name="første"),
        Læg(ref=Ref(name="N", slags=Slags.Tal), value=Tal(value=3)),
        None,
    )


def test_træk():
    assert parse_line(1, "Som det første træk tre fra N") == (
        1,
        Label(name="første"),
        Træk(ref=Ref(name="N", slags=Slags.Tal), value=Tal(value=3)),
        None,
    )


def test_talbeholder():
    assert parse_line(1, "Som det første sæt A til en talbeholder") == (
        1,
        Label("første"),
        Sæt(Ref("A", Slags.TalBeholder), None),
        None,
    )


def test_tekstbeholder():
    assert parse_line(1, "Som det første sæt A til en tekstbeholder") == (
        1,
        Label("første"),
        Sæt(Ref("A", Slags.TekstBeholder), None),
        None,
    )


def test_indsæt():
    assert parse_line(1, "Som det første sæt et i A til fire") == (
        1,
        Label("første"),
        Indsæt(Ref("A", Slags.Afledt), Tal(1), Tal(4)),
        None,
    )


def test_indsæt_ref():
    assert parse_line(1, "Som det første sæt N i A til fire") == (
        1,
        Label("første"),
        Indsæt(Ref("A", Slags.Afledt), Ref("N", Slags.Tal), Tal(4)),
        None,
    )


def test_læs_fra_beholder_statisk():
    assert parse_line(1, "Som det første sæt A til element femten fra B") == (
        1,
        Label("første"),
        Læs(Ref("A", Slags.Afledt), Tal(15), Ref("B", Slags.Afledt)),
        None,
    )


def test_læs_fra_beholder_dynamisk():
    assert parse_line(1, "Som det første sæt A til element N fra B") == (
        1,
        Label("første"),
        Læs(Ref("A", Slags.Afledt), Ref("N", Slags.Tal), Ref("B", Slags.Afledt)),
        None,
    )


def test_hævdelse():
    assert parse_line(1, "Som det første hævd at N er større end to") == (
        1,
        Label("første"),
        Hævdelse(
            Sammenligning(
                negativ=False,
                op=">",
                venstre=Ref(name="N", slags=Slags.Tal),
                højre=Tal(value=2),
            )
        ),
        None,
    )


def test_emma_gad():
    with pytest.raises(Exception) as excinfo:
        parse_line(1, 'Som det første skriv "Hello World"')
    with pytest.raises(Exception) as excinfo:
        parse_line(1, 'Som det første skriv "Hej, Dronning!"')


def test_fortolkning_af_tal():
    assert fortolk_tal("en") == 1
    assert fortolk_tal("ti") == 10
    assert fortolk_tal("tretten") == 13
    with pytest.raises(Exception) as excinfo:
        fortolk_tal("treogti")
    assert fortolk_tal("femoghalvfjerds") == 75
    assert fortolk_tal("firehundredeenogtyve") == 421
    assert fortolk_tal("syvogfirs") == 87
    assert fortolk_tal("tohundredeogtolv") == 212
    assert fortolk_tal("tohundredeogfemogtredive") == 235
    with pytest.raises(Exception) as excinfo:
        fortolk_tal("femtredive")


def test_fortolkning_af_ordenstal():
    assert fortolk_ordenstal("første") == 1
