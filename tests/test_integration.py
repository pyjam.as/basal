import os
import subprocess
from pathlib import Path

from basal import byg


def test_eksempler():
    integrationer = Path(__file__).parent.with_name("eksempler")
    for programmel in integrationer.glob("*.basal"):
        programmel = Path(programmel)
        try:
            hulkortfil = open(programmel.with_suffix(".hulkort"), 'r')
        except FileNotFoundError:
            hulkortfil = None

        # Skip uendelige løkke programmer
        if 'løkke' in programmel.name:
            continue

        bygget_progammel = byg(programmel)
        result = subprocess.run([bygget_progammel], stdin=hulkortfil, stdout=subprocess.PIPE)
        assert result.returncode == 0

        strimmel = result.stdout.decode('utf-8')
        strimmelfilnavn = programmel.with_suffix(".strimmel")
        try:
            with open(strimmelfilnavn, 'r') as strimmelfil:
                assert strimmelfil.read() == strimmel
        except FileNotFoundError:
            if strimmel:
                with open(strimmelfilnavn, 'w') as strimmelfil:
                    strimmelfil.write(strimmel)
