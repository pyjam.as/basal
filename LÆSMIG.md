# Basal

Velkommen til vejledningen til Basal, det sidste skrig indenfor elektronisk
databehandling. Vi finder det en skam at langt størstedelen af beregningssprog
benytter sig af udenlandske idéer, som for eksempel arabiske tal.

Basal gør det let at kommunikere med datamaten og få den til at udføre opgaver,
uden at De behøver at forstå komplekse kommandoer eller udenlandske termer.

Trods den simple model, har Basal høj udtrykskraft:

* Naur-komplette beregninger
* Statiske arter
* Tal, tekst og en-dimensionelle beholdere
* Matematik (plus _og_ minus)
* Læsning fra hulkort
* Skriven til strimler
* Letlæselig ordføjning

Vi håber, at De vil finde glæde og bekvemmelighed i at bruge Basal til Deres
elektroniske databehandlingsbehov. Med Basal bliver programmering en fornøjelse
for alle, og datamatens muligheder åbner sig for alle danskere. God kodning!


## Kom godt i gang

```basal
Som det første skriv "Hej, Verden!"
```


### Ordføjning

Når De skal skrive et programmel i Basal, bruger De naturlige
sprogkonstruktioner, som De allerede er fortrolig med. Den første linje i dit
programmel skal starte med `Som det første` og variabelnavne skal skrives med
versaler.

Følgende underafsnit indeholder eksempler, på hvordan basal bedst bruges. Der
er ydeligere eksempler i `eksempler/`-mappen.


#### Tal

```basal
Som det første sæt N til tre
    sæt M til N
    træk et fra N
    læg fire til M
    skriv N ", " M
    fodnote: dette programmel udskriver "2, 7"
```


#### Tekst

```basal
Som det første sæt HEJ til "Hej"
    skriv HEJ ", verden!"
    fodnote: dette programmel udskriver "Hej, verden!"
```


#### Beholdere

```basal
fodnote: man kan også bruge tekstbeholder
Som det første sæt TAL til en talbeholder
    sæt et i TAL til to
    sæt to i TAL til syv

    sæt A til element et fra TAL
    sæt B til element to fra TAL

    skriv A " " B
    fodnote: dette programmel udskriver "2 7"
```


#### Gå til

```basal
Som det første sæt N til tre
Som det andet skriv "!"
    træk en fra N
    gå til andet hvis N ikke er lig nul
    fodnote: dette programmel udskriver "!!!"
```


#### Spørg

```basal
Som det første spørg "Hvad er dit yndlingstal? " og husk svaret som tallet N
    spørg "Hvad hedder De? " og husk svaret som NAVN
    fodnote: N er nu et tal mens NAVN er tekst
```


### Kørsel

For at oversætte et programmel til maskinkode, skal De benytte den følgende
befaling:

```sh
$ python -m basal byg FILNAVN
```

For at gennemføre et program omgående, skal De benytte den følgende befaling:

```sh
$ python -m basal kør FILNAVN
```


### Systemkrav

* Python 3.10
* MacOS eller Linux
* ld
* nasm
* rustc
