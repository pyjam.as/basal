{
  description = "Programmering for danskere";

  inputs = {
    nixpkgs.url = "nixpkgs/master";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in {
      defaultPackage = pkgs.stdenv.mkDerivation {
        pname = "basalt";
        version = "0.1";
        buildInputs = let
          mPython = pkgs.python310.withPackages (ps: with ps; [black pytest]);
        in [mPython pkgs.rustc pkgs.nasm];
      };
      formatter = pkgs.alejandra;
    });
}
